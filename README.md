# [DevOps for Web Developers](http://url.suncin.me/horchatajs3)

> Repositorio con el código fuente de la demostración.
> Aquí esta el código fuente para realizar el despliegue
> de la aplicación

## Requisitos

Para utilizar este repositorio es necesario tener instalado lo siguiente:
- GIT
- Vagrant
- Cliente SSH
- Editor de texto
- VirtualBox (para pruebas locales)
- Cuenta en DigitalOcean (si no tienes una puedes crear una a través del [enlace](https://m.do.co/c/c51672b031af))

## Aplicación
La aplicación que se va a desplegar es [RealWorld](https://realworld.io/) una
aplicación fullstack de ejemplo que esta disponible de forma libre,
está dividida en dos partes principales el [Frontend](http://url.suncin.me/rw-frontend) y [Backend](http://url.suncin.me/rw-backend)

## Cómo utilizar este repositorio
1. Clonar este repositorio: ``git clone https://gitlab.com/leosuncin/deploy-docker-swarm.git``
2. Entrar al directorio del proyecto: ``cd deploy-docker-swarm``
3. Instalar plugins de Vagrant necesarios: ``vagrant plugin install vagrant-digitalocean vagrant-env``
4. Crear el archivo con las variables de entorno a partir del de ejemplo: ``cp .env{.example,}``
5. Con un editor de texto defina o modifique las variables dentro de `.env`.
6. Sino tiene una clave de SSH creada se debe crear una.
7. Para iniciar la VM en local para pruebas: ``vagrant up``
8. Para iniciar el VPS en DigitalOcean: ``vagrant up --provider=digital_ocean``
