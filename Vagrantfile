Vagrant.configure("2") do |config|
  config.env.enable

  config.vm.define "#{ENV['STACK_NAME']}-devops" do |devops|
    devops.vm.provision "shell" do |s|
      s.path = "provision.sh"
      s.env = {
        "STACK_NAME"                => ENV["STACK_NAME"],
        "DIGITALOCEAN_ACCESS_TOKEN" => ENV["DIGITALOCEAN_ACCESS_TOKEN"],
        "DIGITALOCEAN_REGION"       => ENV["DIGITALOCEAN_REGION"]
      }
    end

    devops.vm.provider :virtualbox do |provider, override|
      override.vm.box = "envimation/ubuntu-xenial-docker"
      provider.memory = 1024
      provider.cpus = 1
    end

    devops.vm.provider :digital_ocean do |provider, override|
      override.ssh.private_key_path = "~/.ssh/id_rsa"
      override.vm.box = "digital_ocean"
      override.vm.box_url = "https://github.com/devopsgroup-io/vagrant-digitalocean/raw/master/box/digital_ocean.box"
      override.nfs.functional = false
      provider.private_networking = true
      provider.monitoring = true
      provider.token = ENV["DIGITALOCEAN_ACCESS_TOKEN"]
      provider.image = "docker-16-04"
      provider.region = ENV["DIGITALOCEAN_REGION"]
      provider.size = "1gb"
    end
  end
end
