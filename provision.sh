#!/bin/bash
set -eo pipefail

DOCKER_COMPOSER_VERSION=${DOCKER_COMPOSER_VERSION:-1.21.2}

get_installed() {
    local pkg=$1
    dpkg-query --show --showformat='${db:Status-Status}\n' $pkg
}

get_ip_address() {
  local interface=$1
  ip addr show $interface | grep "inet\b" | awk '{print $2}' | cut -d/ -f1
}

if [[ $(get_installed docker-ce) = 'not-installed' ]]; then
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
fi

export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get dist-upgrade -y -qq

bash <(curl -Ss https://my-netdata.io/kickstart.sh) all --dont-wait --auto-update
sed -i "\$i echo 1 > /sys/kernel/mm/ksm/run" /etc/rc.local
echo 1 > /sys/kernel/mm/ksm/run
sed -i "\$i echo 1000 > /sys/kernel/mm/ksm/sleep_millisecs\n" /etc/rc.local
echo 1000 > /sys/kernel/mm/ksm/sleep_millisecs

if [[ $(get_installed docker-ce) = 'not-installed' ]]; then
  apt-get install -y docker-ce
fi

if [[ $(docker-compose --version) =~ $DOCKER_COMPOSER_VERSION ]]; then
  curl --silent --show-error --fail https://github.com/docker/compose/releases/download/${DOCKER_COMPOSER_VERSION}/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose
fi

(cd /usr/local/bin && curl https://getmic.ro | bash)

curl -sSL https://rexray.io/install | sh

cat > /etc/rexray/config.yml <<EOL
rexray:
  logLevel: error

libstorage:
  service: dobs
  integration:
    volume:
      operations:
        mount:
          rootPath: /${STACK_NAME}

dobs:
  token: ${DIGITALOCEAN_ACCESS_TOKEN}
  region: ${DIGITALOCEAN_REGION}
EOL

systemctl enable rexray
systemctl start rexray

# enable systemd-journald persistent logs.
sed -i -E 's,^#?(Storage=).*,\1persistent,' /etc/systemd/journald.conf
systemctl restart systemd-journald

mkdir -p /srv/${STACK_NAME} && cd $_
cp /vagrant/{.env,docker-compose.yml,hooks.json,deploy.sh,traefik.toml} .

if [[ ! -f /vagrant/token.txt ]]; then
  docker swarm init --advertise-addr $(get_ip_address eth1) > /vagrant/token.txt
else
  eval $(cat /vagrant/token.txt)
fi

docker network create --driver overlay web
docker stack deploy --compose-file docker-compose.yml $STACK_NAME
